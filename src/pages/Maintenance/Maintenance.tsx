import React from 'react';
import PropTypes from 'prop-types';

// import classes from './Maintenance.module.scss';

type MaintenanceProps = {
  className?: string
}

const Maintenance: React.FC<MaintenanceProps> = ({className}) => {
  return (
    <div className={className}>
      <div>
        <img src="" alt="Logo" />
      </div>
      <div>
        <h2>Sorry, temporarily down!</h2>
        <h3>We&#39;ll be back in no time</h3>
      </div>
    </div>
  );
};

Maintenance.propTypes = {
  className: PropTypes.string
};

export default Maintenance;
